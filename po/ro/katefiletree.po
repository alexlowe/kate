# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2014, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-23 01:00+0000\n"
"PO-Revision-Date: 2020-09-16 00:25+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#: katefiletree.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "Reloa&d"
msgstr "&Reîncarcă"

#: katefiletree.cpp:122
#, kde-format
msgid "Reload selected document(s) from disk."
msgstr "Reîncarcă documentele alese de pe disc."

#: katefiletree.cpp:124
#, kde-format
msgctxt "@action:inmenu"
msgid "Close"
msgstr "Închide"

#: katefiletree.cpp:126
#, kde-format
msgid "Close the current document."
msgstr "Închide documentul actual."

#: katefiletree.cpp:128
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Expand recursively"
msgctxt "@action:inmenu"
msgid "Expand Recursively"
msgstr "Desfășoară recursiv"

#: katefiletree.cpp:130
#, kde-format
msgid "Expand the file list sub tree recursively."
msgstr "Desfășoară recursiv sub-arborele listei de fișiere."

#: katefiletree.cpp:132
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Collapse recursively"
msgctxt "@action:inmenu"
msgid "Collapse Recursively"
msgstr "Restrânge recursiv"

#: katefiletree.cpp:134
#, kde-format
msgid "Collapse the file list sub tree recursively."
msgstr "Restrânge recursiv sub-arborele listei de fișiere."

#: katefiletree.cpp:136
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Other"
msgstr "Închide celelalte"

#: katefiletree.cpp:138
#, kde-format
msgid "Close other documents in this folder."
msgstr "Închide celelalte documente din acest dosar."

#: katefiletree.cpp:141
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Containing Folder"
msgstr "Deschide dosarul conținător"

#: katefiletree.cpp:143
#, kde-format
msgid "Open the folder this file is located in."
msgstr "Deschide dosarul în care se află acest fișier."

#: katefiletree.cpp:145
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Location"
msgstr ""

#: katefiletree.cpp:147
#, kde-format
msgid "Copy path and filename to the clipboard."
msgstr "Copiază calea și denumirea fișierului în clipboard."

#: katefiletree.cpp:149
#, kde-format
msgctxt "@action:inmenu"
msgid "Rename..."
msgstr "Redenumire..."

#: katefiletree.cpp:151
#, kde-format
msgid "Rename the selected file."
msgstr "Redenumește fișierul ales."

#: katefiletree.cpp:154
#, kde-format
msgid "Print selected document."
msgstr "Tipărește documentul ales."

#: katefiletree.cpp:157
#, kde-format
msgid "Show print preview of current document"
msgstr "Arată previzualizarea tipăririi fișierului curent"

#: katefiletree.cpp:159
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Șterge"

#: katefiletree.cpp:161
#, kde-format
msgid "Close and delete selected file from storage."
msgstr "Închide și șterge fișierul ales de pe stocare."

#: katefiletree.cpp:165
#, kde-format
msgctxt "@action:inmenu"
msgid "Clear History"
msgstr "Șterge istoricul"

#: katefiletree.cpp:167
#, kde-format
msgid "Clear edit/view history."
msgstr "Șterge istoricul de redactare/vizualizare."

#: katefiletree.cpp:230
#, kde-format
msgctxt "@action:inmenu"
msgid "Tree Mode"
msgstr "Regim arborescent"

#: katefiletree.cpp:231
#, kde-format
msgid "Set view style to Tree Mode"
msgstr "Stabilește stilul afișării la regimul arborescent"

#: katefiletree.cpp:237
#, kde-format
msgctxt "@action:inmenu"
msgid "List Mode"
msgstr "Regim de listă"

#: katefiletree.cpp:238
#, kde-format
msgid "Set view style to List Mode"
msgstr "Stabilește stilul afișării la regimul de listă"

#: katefiletree.cpp:245
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Name"
msgstr "Denumire document"

#: katefiletree.cpp:246
#, kde-format
msgid "Sort by Document Name"
msgstr "Sortează după denumirea documentului"

#: katefiletree.cpp:251
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Path"
msgstr "Cale document"

#: katefiletree.cpp:251
#, kde-format
msgid "Sort by Document Path"
msgstr "Sortează după calea documentului"

#: katefiletree.cpp:255
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Opening Order"
msgstr "Ordinea de deschidere"

#: katefiletree.cpp:256
#, kde-format
msgid "Sort by Opening Order"
msgstr "Sortează în ordinea deschiderii"

#: katefiletree.cpp:259 katefiletreeconfigpage.cpp:74
#, kde-format
msgid "Custom Sorting"
msgstr ""

#: katefiletree.cpp:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Open With"
msgstr "Deschide cu"

#: katefiletree.cpp:402
#, kde-format
msgid "Show File Git History"
msgstr ""

#: katefiletree.cpp:441
#, kde-format
msgctxt "@action:inmenu"
msgid "View Mode"
msgstr "Regim de vizualizare"

#: katefiletree.cpp:445
#, kde-format
msgctxt "@action:inmenu"
msgid "Sort By"
msgstr "Sortează după"

#: katefiletreeconfigpage.cpp:45
#, kde-format
msgid "Background Shading"
msgstr "Nuanțe de fundal"

#: katefiletreeconfigpage.cpp:52
#, kde-format
msgid "&Viewed documents' shade:"
msgstr "Fundalul documentelor &vizualizate:"

#: katefiletreeconfigpage.cpp:58
#, kde-format
msgid "&Modified documents' shade:"
msgstr "Fundalul documentelor &modificate:"

#: katefiletreeconfigpage.cpp:66
#, kde-format
msgid "&Sort by:"
msgstr "&Sortează după:"

#: katefiletreeconfigpage.cpp:71
#, kde-format
msgid "Opening Order"
msgstr "Ordinea de deschidere"

#: katefiletreeconfigpage.cpp:72
#, kde-format
msgid "Document Name"
msgstr "Denumire document"

#: katefiletreeconfigpage.cpp:73
#, kde-format
msgid "Url"
msgstr "Url"

#: katefiletreeconfigpage.cpp:79
#, kde-format
msgid "&View Mode:"
msgstr "Regim de &vizualizare:"

#: katefiletreeconfigpage.cpp:84
#, kde-format
msgid "Tree View"
msgstr "Vizualizare arborescentă"

#: katefiletreeconfigpage.cpp:85
#, kde-format
msgid "List View"
msgstr "Vizualizare listă"

#: katefiletreeconfigpage.cpp:90
#, kde-format
msgid "&Show Full Path"
msgstr "&Arată calea completă"

#: katefiletreeconfigpage.cpp:95
#, fuzzy, kde-format
#| msgid "Main Toolbar"
msgid "Show &Toolbar"
msgstr "Bara de unelte principală"

#: katefiletreeconfigpage.cpp:98
#, kde-format
msgid "Show Close Button"
msgstr ""

#: katefiletreeconfigpage.cpp:100
#, kde-format
msgid ""
"When enabled, this will show a close button for opened documents on hover."
msgstr ""

#: katefiletreeconfigpage.cpp:105
#, kde-format
msgid ""
"When background shading is enabled, documents that have been viewed or "
"edited within the current session will have a shaded background. The most "
"recent documents have the strongest shade."
msgstr ""
"Când sunt activate nuanțele de fundal, documentele care au fost vizualizate "
"sau redactate în sesiunea curentă vor avea un fundal colorat. Documentele "
"recente au nuanța cea mai intensă."

#: katefiletreeconfigpage.cpp:108
#, kde-format
msgid "Set the color for shading viewed documents."
msgstr "Stabilește culoarea pentru fundalul documentelor vizualizate."

#: katefiletreeconfigpage.cpp:110
#, kde-format
msgid ""
"Set the color for modified documents. This color is blended into the color "
"for viewed files. The most recently edited documents get most of this color."
msgstr ""
"Stabilește culoarea pentru fundalul documentelor modificate. Această culoare "
"este amestecată cu culoarea fișierelor vizualizate. Cele mai recent "
"redactate documente au această culoare într-o nuanță mai intensă."

#: katefiletreeconfigpage.cpp:115
#, kde-format
msgid ""
"When enabled, in tree mode, top level folders will show up with their full "
"path rather than just the last folder name."
msgstr ""
"Când este activat, în regim arborescent, dosarele primului nivel vor fi "
"afișate cu calea completă împreună cu denumirea ultimului dosar."

#: katefiletreeconfigpage.cpp:118
#, kde-format
msgid ""
"When enabled, a toolbar with actions like “Save” are displayed above the "
"list of documents."
msgstr ""

#: katefiletreeconfigpage.cpp:137 katefiletreeplugin.cpp:129
#: katefiletreeplugin.cpp:136
#, kde-format
msgid "Documents"
msgstr "Documente"

#: katefiletreeconfigpage.cpp:142
#, kde-format
msgid "Configure Documents"
msgstr "Configurare documente"

#: katefiletreemodel.cpp:493
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Open With"
msgctxt ""
"Open here is a description, i.e. 'list of widgets that are open' not a verb"
msgid "Open Widgets"
msgstr "Deschide cu"

#: katefiletreemodel.cpp:647
#, kde-format
msgctxt "%1 is the full path"
msgid ""
"<p><b>%1</b></p><p>The document has been modified by another application.</p>"
msgstr ""
"<p><b>%1</b></p><p>Documentul a fost modificat de către o altă aplicație.</p>"

#: katefiletreeplugin.cpp:246
#, kde-format
msgid "Previous Document"
msgstr "Documentul precedent"

#: katefiletreeplugin.cpp:252
#, kde-format
msgid "Next Document"
msgstr "Documentul următor"

#: katefiletreeplugin.cpp:258
#, fuzzy, kde-format
#| msgid "&Show Active"
msgid "&Show Active Document"
msgstr "&Arată documentul activ"

#: katefiletreeplugin.cpp:263
#, kde-format
msgid "Save"
msgstr ""

#: katefiletreeplugin.cpp:264
#, kde-format
msgid "Save the current document"
msgstr "Salvează documentul curent"

#: katefiletreeplugin.cpp:268
#, kde-format
msgid "Save As"
msgstr ""

#: katefiletreeplugin.cpp:269
#, fuzzy, kde-format
#| msgid "Save current document under new name"
msgid "Save the current document under a new name"
msgstr "Salvează documentul actual cu denumire nouă"

#. i18n: ectx: Menu (go)
#: ui.rc:7
#, kde-format
msgid "&Go"
msgstr ""

#~ msgid "&Other..."
#~ msgstr "&Altceva..."

#~ msgid "Do you really want to delete file \"%1\" from storage?"
#~ msgstr "Sigur doriți să ștergeți fișierul „%1” de pe stocare?"

#~ msgid "Delete file?"
#~ msgstr "Ștergeți fișierul?"

#~ msgid "File \"%1\" could not be deleted."
#~ msgstr "Fișierul „%1” nu a putut fi șters."

#~ msgid "Rename file"
#~ msgstr "Redenumește fișierul"

#~ msgid "New file name"
#~ msgstr "Denumire nouă de fișier"

#~ msgid "File \"%1\" could not be moved to \"%2\""
#~ msgstr "Fișierul „%1” nu a putut fi mutat la „%2”"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sergiu Bivol"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sergiu@cip.md"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Close Other"
#~ msgid "Close Widget"
#~ msgstr "Închide celelalte"

#~ msgid "Kate File Tree"
#~ msgstr "Arbore de fișiere Kate"

#~ msgctxt "@action:inmenu"
#~ msgid "Copy File Path"
#~ msgstr "Copiază calea fișierului"

#~ msgid "&View"
#~ msgstr "&Vizualizare"

#~ msgid "Save Current Document"
#~ msgstr "Salvează documentul actual"

#~ msgid "Save Current Document As"
#~ msgstr "Salvează documentul actual ca"

#~ msgid "Application '%1' not found."
#~ msgstr "Aplicația „%1” nu a fost găsită."

#~ msgid "Application not found"
#~ msgstr "Aplicația nu a fost gasită"

#~ msgctxt "@action:inmenu"
#~ msgid "Delete Document"
#~ msgstr "Șterge documentul"
