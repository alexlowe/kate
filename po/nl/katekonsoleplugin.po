# translation of katekonsoleplugin.po to Dutch
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Rinse de Vries <rinsedevries@kde.nl>, 2007.
# Kristof Bal <kristof.bal@gmail.com>, 2009.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2009, 2013, 2014, 2017, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-16 01:02+0000\n"
"PO-Revision-Date: 2021-05-01 12:13+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kateconsole.cpp:54
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "U hebt onvoldoende rechten om een shell of terminal te openen."

#: kateconsole.cpp:102 kateconsole.cpp:132 kateconsole.cpp:656
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:141
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Pipe naar terminal"

#: kateconsole.cpp:145
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "Terminal met huidig document s&ynchroniseren"

#: kateconsole.cpp:149
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Huidig document uitvoeren"

#: kateconsole.cpp:154 kateconsole.cpp:504
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Terminalpaneel &tonen"

#: kateconsole.cpp:160
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Focus op terminalpaneel"

#: kateconsole.cpp:373
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Wilt u de tekst via een pipe naar de console sturen? Dit zal alle commando's "
"die het bevat uitvoeren onder uw gebruikersrechten."

#: kateconsole.cpp:374
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Naar terminal verzenden?"

#: kateconsole.cpp:375
#, kde-format
msgid "Pipe to Terminal"
msgstr "Naar terminal verzenden"

#: kateconsole.cpp:403
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Kan map '%1' niet binnengaan"

#: kateconsole.cpp:445
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Geen lokaal bestand: '%1'"

#: kateconsole.cpp:478
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Wilt u het document uitvoeren?\n"
"Dit zal het volgende commando uitvoeren,\n"
"met uw gebruikersrechten, in de terminal:\n"
"'%1'"

#: kateconsole.cpp:485
#, kde-format
msgid "Run in Terminal?"
msgstr "In terminal uitvoeren?"

#: kateconsole.cpp:486
#, kde-format
msgid "Run"
msgstr "Uitvoeren"

#: kateconsole.cpp:501
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "Terminalpaneel &verbergen"

#: kateconsole.cpp:512
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Focus van terminalpaneel weghalen"

#: kateconsole.cpp:513 kateconsole.cpp:514
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Focus op terminalpaneel"

#: kateconsole.cpp:589
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"De terminal &automatisch synchroniseren met het huidige document, wanneer "
"mogelijk"

#: kateconsole.cpp:593 kateconsole.cpp:614
#, kde-format
msgid "Run in terminal"
msgstr "In terminal uitvoeren"

#: kateconsole.cpp:595
#, kde-format
msgid "&Remove extension"
msgstr "Extensie ve&rwijderen"

#: kateconsole.cpp:600
#, kde-format
msgid "Prefix:"
msgstr "Voorvoegsel:"

#: kateconsole.cpp:608
#, kde-format
msgid "&Show warning next time"
msgstr "De volgende keer waarschuwing &tonen"

#: kateconsole.cpp:610
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"De volgende keer dat '%1' wordt uitgevoerd, ga dan na dat er een "
"waarschuwingsvenster verschijnt, die het commando toont dat wordt verzonden "
"naar de terminal, om te bekijken."

#: kateconsole.cpp:621
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Stel de omgevingsvariabele &EDITOR in op 'kate -b'"

#: kateconsole.cpp:624
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Belangrijk: Het document moet gesloten zijn om het console-programma door te "
"laten gaan"

#: kateconsole.cpp:627
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Konsole verbergen bij indrukken van 'Esc'"

#: kateconsole.cpp:630
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Dit kan problemen veroorzaken met terminaltoepassingen die de toets Esc "
"gebruiken, voor bijv., vim. Voeg deze toepassingen toe aan de onderstaande "
"invoer (kommagescheiden lijst)"

#: kateconsole.cpp:661
#, kde-format
msgid "Terminal Settings"
msgstr "Terminalinstellingen"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "H&ulpmiddelen"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Rinse de Vries,Freek de Kruijf - 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "rinsedevries@kde.nl,freekdekruijf@kde.nl"

#~ msgid "Kate Terminal"
#~ msgstr "Kate Terminal"

#~ msgid "Terminal Panel"
#~ msgstr "Terminalpaneel"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgid "Embedded Konsole"
#~ msgstr "Ingebedde Konsole"
